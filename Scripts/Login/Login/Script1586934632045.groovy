import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import internal.GlobalVariable as GlobalVariable

//def appPath = PathUtil.relativeToAbsolutePath(GlobalVariable.G_AndroidApp, RunConfiguration.getProjectDir())
'Get full directory’s path of android application'
Mobile.startApplication('C:\\Users\\ASANI\\Desktop\\APK\\app-releaseStaging.apk', false)

Mobile.waitForElementPresent(findTestObject('Login/Button_Humburger'), 30)

Mobile.tap(findTestObject('Login/Button_Humburger'), 30)

Mobile.tap(findTestObject('Login/button_Login'), 10)

Mobile.setText(findTestObject('Login/input_EmailPhone'), '083899197368', 10)

Mobile.setEncryptedText(findTestObject('Login/input_Password'), 'aeHFOx8jV/A=', 10)

Mobile.tap(findTestObject('Login/Button_sign in'), 10)

Mobile.tap(findTestObject('Login/Button_Humburger'), 10)

Mobile.tap(findTestObject('Humberger Page/Button_Edit Profile'), 10)

Mobile.tap(findTestObject('My Profile Page/Button_Log out'), 10)

Mobile.tap(findTestObject('My Profile Page/Button_OK Confirmation'), 10)

Mobile.closeApplication()

