import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication('C:\\Users\\ASANI\\Desktop\\APK\\app-releaseUat.apk', false)

Mobile.waitForElementPresent(findTestObject('Login/Button_Humburger'), 30)

Mobile.tap(findTestObject('Login/Button_Humburger'), 30)

Mobile.tap(findTestObject('Register/Button_Register Now'), 10)

Mobile.setText(findTestObject('Login/input_EmailPhone'), '081311112222', 10)

Mobile.tap(findTestObject('Register/Button Submit'), 10)

Mobile.tap(findTestObject('Register/Select_Full Name'), 10, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Register/Choose_Mr.'), 10, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Register/input_Full Name'), '081311112222', 10)

Mobile.setText(findTestObject('Register/input_Last Name'), '081311112222', 10)

Mobile.setText(findTestObject('Register/input_email Regist'), '081311112222', 10)

Mobile.setText(findTestObject('Register/input_Mobile Number Regist'), '081311112222', 10)

Mobile.setText(findTestObject('Register/input_password (1)'), '081311112222', 10)

Mobile.setText(findTestObject('Register/input_Retype Password'), '081311112222', 10)

