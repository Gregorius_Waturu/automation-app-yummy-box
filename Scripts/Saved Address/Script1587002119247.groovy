import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

'Get full directory’s path of android application'
Mobile.startApplication('C:\\Users\\ASANI\\Desktop\\APK\\app-releaseStaging.apk', false)

not_run: Mobile.waitForElementPresent(findTestObject('Login/Button_Humburger'), 30)

not_run: Mobile.tap(findTestObject('Login/Button_Humburger'), 30)

not_run: Mobile.tap(findTestObject('Login/button_Login'), 10)

not_run: Mobile.sendKeys(findTestObject('Login/input_EmailPhone'), '083899197368', FailureHandling.STOP_ON_FAILURE)

not_run: Mobile.setEncryptedText(findTestObject('Login/input_Password'), 'aeHFOx8jV/A=', 10)

not_run: Mobile.tap(findTestObject('Login/Button_sign in'), 10)

Mobile.tap(findTestObject('Login/Button_Humburger'), 10)

Mobile.tap(findTestObject('Humberger Page/Button_Edit Profile'), 10)

Mobile.tap(findTestObject('My Profile Page/Page_Saved Addresses'), 10)

Mobile.tap(findTestObject('My Profile Page/a_Saved Address/Button_Add New Address'), 10)

Mobile.tap(findTestObject('My Profile Page/a_Saved Address/input_Search Address'), 10)

Mobile.sendKeys(findTestObject('My Profile Page/a_Saved Address/input_Search Address'), 'Jakarta', FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

Mobile.tap(findTestObject('My Profile Page/a_Saved Address/button_Address Jakarta'), 10)

Mobile.sendKeys(findTestObject('My Profile Page/a_Saved Address/input_Address Label'), 'Jakarta', FailureHandling.STOP_ON_FAILURE)

not_run: Mobile.closeApplication()

